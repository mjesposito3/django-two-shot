from django.contrib import admin
from receipts.models import Receipts, Categories, Accounts

# Register your models here.


class ReceiptAdmin(admin.ModelAdmin):
	pass


class CategoryAdmin(admin.ModelAdmin):
	pass


class AccountAdmin(admin.ModelAdmin):
	pass


admin.site.register(Receipts, ReceiptAdmin)
admin.site.register(Categories, CategoryAdmin)
admin.site.register(Accounts, AccountAdmin)