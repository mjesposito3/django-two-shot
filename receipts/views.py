from distutils.log import Log
from multiprocessing import context
from unicodedata import category
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from receipts.models import Accounts, Categories, Receipts

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipts
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipts.objects.filter(purchaser=self.request.user)
    
    
class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipts
    template_name = "receipts/create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account", ]
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")


class AccountListView(LoginRequiredMixin, ListView):
    model = Accounts
    template_name = "accounts/list.html"
    
    def get_queryset(self):
        return Accounts.objects.filter(owner=self.request.user)


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Accounts
    template_name = "accounts/create.html"
    fields = ["name", "number"]
    
    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("account_list")


class CategoryListView(LoginRequiredMixin, ListView):
    model = Categories
    template_name = "categories/list.html"

    def get_queryset(self):
        return Categories.objects.filter(owner=self.request.user)


class CategoryCreateView(LoginRequiredMixin, CreateView):
    model = Categories
    template_name = "categories/create.html"
    fields = ["name",]
    
    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("category_list")
